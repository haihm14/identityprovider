﻿using IdentityProvider.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityProvider.Repository
{
    public interface IApplicationRepository : IRepository<Applications>
    {
    }
}
