﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityProvider.Repository
{
    public interface IRepository<T>
    {
        int Add(T item);
        int Edit(T item);
        bool LockAndActive(T item);
        bool Delete(int id);
        List<T> GetList();
        T GetById(int id);
    }
}
