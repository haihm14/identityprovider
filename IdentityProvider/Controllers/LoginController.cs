﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using IdentityProvider.Common;
using IdentityProvider.ViewModels;

namespace IdentityProvider.Controllers
{
    public class LoginController : Controller
    {
        private readonly IConfiguration _configuration;

        public LoginController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public ActionResult Index()
        {
            //var key = _configuration["AES256:Key"];
            return View();
        }

        [HttpPost]
        public IActionResult CheckLogin([FromBody]UserLoginViewModel model)
        {
            var response = new {a = 1};
            var a = model;
            return StatusCode(200, response);
        }

    }
}