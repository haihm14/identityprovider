﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IdentityProvider.Models;
using IdentityProvider.Models.DataAccess;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace IdentityProvider.Controllers
{
    public class UsersController : BaseController
    {
        private MASTERContext _context;
        private UserDA _da;

        public UsersController(MASTERContext context)
        {
            _context = context;
            _da = new UserDA(_context);
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            Users user = new Users();
            user.Username = "haihm";
            user.Password = "HMH";
            user.IsDelete = false;
            user.IsActive = true;
            user.Salt = "Salt";
            //Test Add
            int add = _da.Add(user);

            Users userEdit = _da.GetById(3);
            userEdit.FullName = "member Red ";
            int editResult = _da.Edit(userEdit);

            List<Users> lst = _da.GetList();
            return View(lst);
            //return View(await _context.Users.ToListAsync());
        }

        #region Default Generic
        //// GET: Users/Details/5
        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var users = await _context.Users
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (users == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(users);
        //}

        //// GET: Users/Create
        //public IActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Users/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("Id,Username,FullName,Code,PhoneNumber,Email,Password,Salt,FromDate,ToDate,IsOnline,IsActive,LastLoginDate,IsDelete,Address,Avatar,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,CountLoginFail,Gender,BirthDay")] Users users)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.Add(users);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(users);
        //}

        //// GET: Users/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var users = await _context.Users.FindAsync(id);
        //    if (users == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(users);
        //}

        //// POST: Users/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("Id,Username,FullName,Code,PhoneNumber,Email,Password,Salt,FromDate,ToDate,IsOnline,IsActive,LastLoginDate,IsDelete,Address,Avatar,CreatedBy,CreatedDate,UpdatedBy,UpdatedDate,CountLoginFail,Gender,BirthDay")] Users users)
        //{
        //    if (id != users.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(users);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!UsersExists(users.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(users);
        //}

        //// GET: Users/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var users = await _context.Users
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (users == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(users);
        //}

        //// POST: Users/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var users = await _context.Users.FindAsync(id);
        //    _context.Users.Remove(users);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        //private bool UsersExists(int id)
        //{
        //    return _context.Users.Any(e => e.Id == id);
        //}
        #endregion

        // <summary>
        /// HaiHM
        /// Upload Avatar
        /// </summary>
        /// <param name="files">file</param>
        /// <returns></returns>
        //[Route("~/api/Account/UploadFile")]
        //[HttpPost]
        //[Authorize]
        //public IActionResult UploadFile(IFormFile files)
        //{
        
        //        if (files.Length > 0)
        //        {
             
        //            try
        //            {
        //                // Get Directory Organization
        //                if (!Directory.Exists(_environment.WebRootPath + AccountConstant.DirectoryUploads + org.OrganizationCode + AccountConstant.DirectorySlat))
        //                {
        //                    Directory.CreateDirectory(_environment.WebRootPath + AccountConstant.DirectoryUploads + org.OrganizationCode + AccountConstant.DirectorySlat);
        //                }
        //                using (FileStream filestream = System.IO.File.Create(_environment.WebRootPath + AccountConstant.DirectoryUploads + org.OrganizationCode + AccountConstant.DirectorySlat + files.FileName))
        //                {
        //                    files.CopyTo(filestream);
        //                    filestream.Flush();
        //                    string str = "";
        //                    var obj = new { message = str };
        //                    return StatusCode(201, obj);
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                return BadRequest();
        //            }
        //        }
        //}
    }
}
