﻿using System;
using System.Collections.Generic;

namespace IdentityProvider.Models
{
    public partial class Applications
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
