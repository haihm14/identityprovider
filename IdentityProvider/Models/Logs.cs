﻿using System;
using System.Collections.Generic;

namespace IdentityProvider.Models
{
    public partial class Logs
    {
        public int Id { get; set; }
        public int? AppId { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public string ActionLog { get; set; }
        public DateTime? ActionDate { get; set; }
        public string RemoteIp { get; set; }
        public string ActionResult { get; set; }
    }
}
