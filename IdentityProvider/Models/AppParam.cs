﻿using System;
using System.Collections.Generic;

namespace IdentityProvider.Models
{
    public partial class AppParam
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int? Position { get; set; }
        public string Description { get; set; }
        public bool? IsLock { get; set; }
        public bool? IsDelete { get; set; }
        public int? Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
