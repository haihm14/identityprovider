﻿using System;
using System.Collections.Generic;

namespace IdentityProvider.Models
{
    public partial class AppGroupRole
    {
        public int Id { get; set; }
        public int AppId { get; set; }
        public int GroupId { get; set; }
        public int RoleId { get; set; }
        public int? Status { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
