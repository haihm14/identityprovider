﻿using IdentityProvider.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityProvider.Models.DataAccess
{
    public class RoleDA : IRoleRepository
    {
        private MASTERContext db;

        public RoleDA(MASTERContext context)
        {
            db = context;
        }

        public int Add(Roles item)
        {
            int addResult = 0;
            // check username
            Roles rChect = db.Roles.Where(r => r.Code.Equals(item.Code) == true).FirstOrDefault();
            if (rChect != null)
                return addResult;
            try
            {
                db.Add(item);
                db.SaveChanges();
                addResult = 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                addResult = 2; // Exception
            }
            return addResult;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int Edit(Roles item)
        {
            int editResult = 0;
            Roles rChect = db.Roles.Where(u => u.Code.Equals(item.Code) == true && u.Id == item.Id).FirstOrDefault();
            if (rChect != null)
            {
                try
                {
                    db.Update(item);
                    db.SaveChanges();
                    editResult = 1;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    editResult = 2; // Exception
                }
            }
            return editResult;

        }

        public Roles GetById(int id)
        {
            try
            {
                return db.Roles.Where(u => u.Id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public List<Roles> GetList()
        {
            try
            {
                return db.Roles.Where(u => u.IsDelete == false).ToList<Roles>();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public bool LockAndActive(Roles item)
        {
            bool result = false;
            Roles rChect = db.Roles.Where(u => u.Code.Equals(item.Code) == true && u.Id == item.Id).FirstOrDefault();
            if (rChect != null)
            {
                try
                {
                    db.Update(rChect);
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    result = false;
                }
            }
            return result;
        }
    }
}
