﻿using IdentityProvider.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityProvider.Models.DataAccess
{
    public class AppParamDA : IAppParamRepository
    {
        private MASTERContext db;

        public AppParamDA(MASTERContext context)
        {
            db = context;
        }

        public int Add(AppParam item)
        {
            int addResult = 0;
            // check username
            AppParam check = db.AppParam.Where(r => r.Code.Equals(item.Code) == true).FirstOrDefault();
            if (check != null)
                return addResult;
            try
            {
                db.Add(item);
                db.SaveChanges();
                addResult = 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                addResult = 2; // Exception
            }
            return addResult;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int Edit(AppParam item)
        {
            int editResult = 0;
            AppParam check = db.AppParam.Where(u => u.Code.Equals(item.Code) == true && u.Id == item.Id).FirstOrDefault();
            if (check != null)
            {
                try
                {
                    db.Update(item);
                    db.SaveChanges();
                    editResult = 1;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    editResult = 2; // Exception
                }
            }
            return editResult;

        }

        public AppParam GetById(int id)
        {
            try
            {
                return db.AppParam.Where(u => u.Id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public List<AppParam> GetList()
        {
            try
            {
                return db.AppParam.Where(u => u.IsDelete == false).ToList<AppParam>();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public bool LockAndActive(AppParam item)
        {
            bool result = false;
            AppParam check = db.AppParam.Where(u => u.Code.Equals(item.Code) == true && u.Id == item.Id).FirstOrDefault();
            if (check != null)
            {
                try
                {
                    db.Update(check);
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    result = false;
                }
            }
            return result;
        }
    }
}
