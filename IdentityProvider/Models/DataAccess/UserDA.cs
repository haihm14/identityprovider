﻿using IdentityProvider.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityProvider.Models.DataAccess
{
    public class UserDA : IUserRepository
    {
        private MASTERContext db;

        public UserDA(MASTERContext context)
        {
            db = context;
        }

        public int Add(Users item)
        {
            int addResult = 0;
            // check username
            Users uChect = db.Users.Where(u => u.Username.Equals(item.Username) == true).FirstOrDefault();
            if (uChect != null)
                return addResult;
            try
            {
                db.Add(item);
                db.SaveChanges();
                addResult = 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                addResult = 2; // Exception
            }
            return addResult;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int Edit(Users item)
        {
            int editResult = 0;
            Users uChect = db.Users.Where(u => u.Username.Equals(item.Username) == true && u.Id == item.Id).FirstOrDefault();
            if(uChect != null)
            {
                try
                {
                    db.Update(item);
                    db.SaveChanges();
                    editResult = 1;
                }catch(Exception ex)
                {
                    Console.WriteLine(ex);
                    editResult = 2; // Exception
                }
            }
            return editResult;
           
        }

        public Users GetById(int id)
        {
            try
            {
                return db.Users.Where(u => u.Id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public List<Users> GetList()
        {
            try
            {
                return db.Users.Where(u => u.IsDelete == false).ToList<Users>();
            }catch(Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public bool LockAndActive(Users item)
        {
            bool result = false;
            Users uChect = db.Users.Where(u => u.Username.Equals(item.Username) == true && u.Id == item.Id).FirstOrDefault();
            if (uChect != null)
            {
                try
                {
                    db.Update(uChect);
                    db.SaveChanges();
                    result = true;
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex);
                    result = false;
                }
            }
            return result;
        }
    }
}
