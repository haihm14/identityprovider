﻿using IdentityProvider.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityProvider.Models.DataAccess
{
    public class GroupDA : IGroupRepository
    {
        private MASTERContext db;

        public GroupDA(MASTERContext context)
        {
            db = context;
        }

        public int Add(Groups item)
        {
            int addResult = 0;
            // check username
            Groups gChect = db.Groups.Where(r => r.Code.Equals(item.Code) == true).FirstOrDefault();
            if (gChect != null)
                return addResult;
            try
            {
                db.Add(item);
                db.SaveChanges();
                addResult = 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                addResult = 2; // Exception
            }
            return addResult;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int Edit(Groups item)
        {
            int editResult = 0;
            Groups gChect = db.Groups.Where(u => u.Code.Equals(item.Code) == true && u.Id == item.Id).FirstOrDefault();
            if (gChect != null)
            {
                try
                {
                    db.Update(item);
                    db.SaveChanges();
                    editResult = 1;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    editResult = 2; // Exception
                }
            }
            return editResult;

        }

        public Groups GetById(int id)
        {
            try
            {
                return db.Groups.Where(u => u.Id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public List<Groups> GetList()
        {
            try
            {
                return db.Groups.Where(u => u.IsDelete == false).ToList<Groups>();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public bool LockAndActive(Groups item)
        {
            bool result = false;
            Groups gChect = db.Groups.Where(u => u.Code.Equals(item.Code) == true && u.Id == item.Id).FirstOrDefault();
            if (gChect != null)
            {
                try
                {
                    db.Update(gChect);
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    result = false;
                }
            }
            return result;
        }
    }
}
