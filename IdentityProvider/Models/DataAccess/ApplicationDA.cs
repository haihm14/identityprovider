﻿using IdentityProvider.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityProvider.Models.DataAccess
{
    public class ApplicationDA : IApplicationRepository
    {
        private MASTERContext db;

        public ApplicationDA(MASTERContext context)
        {
            db = context;
        }

        public int Add(Applications item)
        {
            int addResult = 0;
            // check username
            Applications check = db.Applications.Where(r => r.Code.Equals(item.Code) == true).FirstOrDefault();
            if (check != null)
                return addResult;
            try
            {
                db.Add(item);
                db.SaveChanges();
                addResult = 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                addResult = 2; // Exception
            }
            return addResult;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int Edit(Applications item)
        {
            int editResult = 0;
            Applications check = db.Applications.Where(u => u.Code.Equals(item.Code) == true && u.Id == item.Id).FirstOrDefault();
            if (check != null)
            {
                try
                {
                    db.Update(item);
                    db.SaveChanges();
                    editResult = 1;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    editResult = 2; // Exception
                }
            }
            return editResult;

        }

        public Applications GetById(int id)
        {
            try
            {
                return db.Applications.Where(u => u.Id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public List<Applications> GetList()
        {
            try
            {
                return db.Applications.Where(u => u.Status == 0).ToList<Applications>();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public bool LockAndActive(Applications item)
        {
            bool result = false;
            Applications check = db.Applications.Where(u => u.Code.Equals(item.Code) == true && u.Id == item.Id).FirstOrDefault();
            if (check != null)
            {
                try
                {
                    db.Update(check);
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    result = false;
                }
            }
            return result;
        }
    }
}
