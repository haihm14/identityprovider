﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityProvider.ViewModels
{
    public class UserLoginViewModel
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
